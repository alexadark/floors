<?php
// css dev mode.
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_586e52b01e7b4',
		'title' => 'Css dev mode',
		'fields' => array (
			array (
				'default_value' => 0,
				'message' => '',
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
				'key' => 'field_586e52bf0a476',
				'label' => 'Activate css dev mode',
				'name' => 'wst_css_dev_mode',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;
