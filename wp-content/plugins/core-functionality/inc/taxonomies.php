<?php
add_action('init', 'register_floor_categories_taxonomy');

function register_floor_categories_taxonomy(){

	$labels = array(
		'name'          => _x( ' Floor Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Floor Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Floor Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Floor Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Floor Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Floor Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Floor Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Floor Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('floor-categories','floors', $args);
}

add_action('init', 'register_galleries_taxonomy');

function register_galleries_taxonomy(){

	$labels = array(
		'name'          => _x( ' Galleries', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Gallery', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Galleries', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Galleries', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Gallery', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Gallery', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Gallery', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Gallery', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('galleries','floors', $args);
}

add_action('init', 'register_floor_types_taxonomy');

function register_floor_types_taxonomy(){

	$labels = array(
		'name'          => _x( ' Floor Types', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Floor Type', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Floor Types', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Floor Types', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Floor Type', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Floor Type', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Floor Type', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Floor Type', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('floor-types','floors', $args);
}