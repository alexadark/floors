<?php
beans_remove_markup( 'beans_archive_title' );
beans_remove_output( 'beans_archive_title_text' );
beans_remove_markup( 'beans_post_header' );
beans_remove_action( 'beans_post_title' );
beans_remove_markup( 'beans_post' );
beans_remove_markup( 'beans_post_body' );
beans_remove_markup( 'beans_post_content' );
beans_remove_action( 'beans_post_image' );

beans_load_document();