<?php
function get_four_boxes() {
	$context              = Timber::get_context();
	$context['galleries'] = Timber::get_terms( 'galleries' );
	$templates            = array( '_four-boxes.twig' );
	Timber::render( $templates, $context );
}


function get_testimonials() {
	$context                 = Timber::get_context();
	$context['testimonials'] = Timber::get_posts( array( "post_type" => "testimonials" ) );
	$templates               = array( '_testimonials.twig' );
	Timber::render( $templates, $context );
}

function wst_modify_post_content( $content ) {

	if ( is_singular()) {
		return $content;
	}
	$content = '<div class="post-excerpt">' . wp_trim_words( get_the_content(), 100, '...' ) . '</div>';

	return $content. '<div class="more-link">' . beans_post_more_link() . '</div>';
}




