<?php
//beans_remove_action( 'beans_post_content' );

beans_add_smart_action( 'beans_content_prepend_markup', 'get_cat_filters' );

function get_cat_filters() {
	$context               = Timber::get_context();
	$context['categories'] = Timber::get_terms( 'categories' );
	$templates             = array( 'cat-filter.twig' );
	Timber::render( $templates, $context );
}



//beans_add_smart_action( 'beans_post_prepend_markup', 'get_post_content_view' );
function get_post_content_view() {
	$context   = Timber::get_context();
	$context['articles'] = new TimberPost();
	$templates = array( 'post-content.twig' );
	Timber::render( $templates, $context );
	
}


beans_load_document();