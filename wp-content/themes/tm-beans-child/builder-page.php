<?php
//Template name: builder page
beans_remove_attribute('beans_fixed_wrap_main','class','uk-container uk-container-center');
beans_remove_attribute('beans_post','class','uk-panel-box');

add_action( 'beans_post_content_prepend_markup', 'wst_display_builder' );
function wst_display_builder() {
function wst_display_blocks() {
			$context         = Timber::get_context(); 
			$post           = new TimberPost();
			$context['post'] = $post;
			$templates       = array( 'blocks.twig' );
			Timber::render( $templates, $context ); 
		}
}

beans_load_document();