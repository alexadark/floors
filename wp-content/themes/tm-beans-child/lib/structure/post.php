<?php

beans_add_smart_action( 'wp', 'wst_set_up_post_structure' );
function wst_set_up_post_structure() {
	beans_remove_markup('beans_archive_title');
	beans_remove_output('beans_archive_title_text');
	//Remove title only on pages
	if ( is_page() ) {
		beans_remove_action( 'beans_post_title' );
	}
	if ( ! is_front_page() && (!is_home()) && (!is_archive('testimonials')) ) {

		beans_add_smart_action( 'beans_footer_before_markup', 'get_testimonials',2 );
	}
	if(!is_front_page()){
		beans_add_smart_action( 'beans_footer_before_markup', 'get_four_boxes',1 );
	}
	if ( is_home() || is_category() ) {
		add_filter( 'the_content', 'wst_modify_post_content' );
		beans_remove_action( 'beans_post_image' );
		beans_add_smart_action( 'beans_post_header_append_markup', 'display_post_image' );
		function display_post_image() {
			the_post_thumbnail( 'medium' );
		}


	}
}
