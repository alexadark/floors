<?php
beans_add_smart_action( 'wp', 'wst_set_up_footer_structure' );
function wst_set_up_footer_structure() {
	// Overwrite the Footer content

	beans_modify_action_callback( 'beans_footer_content', 'beans_child_footer_content' );

	function beans_child_footer_content() {
		$context                = Timber::get_context(); //what is default role of this template ?
		$context['year']        = date( 'Y' );
		$templates              = array( 'footer.twig' );
		Timber::render( $templates, $context ); //make ingredients ($context) available to this template
	}
}

