<?php
beans_add_smart_action( 'wp', 'wst_set_up_header_structure' );
function wst_set_up_header_structure() {

	beans_remove_action( 'beans_site_title_tag' );

	//sticky header
	beans_add_attribute( 'beans_header', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top'}" );
	//center menu
	beans_remove_attribute( 'beans_site_branding', 'class', 'uk-float-left' );
	beans_replace_attribute( 'beans_primary_menu', 'class', 'uk-float-right', 'uk-text-center' );
	beans_add_attribute( 'beans_menu[_navbar][_primary]', 'class', 'uk-display-inline-block' );
	beans_add_attribute( 'beans_menu_item', 'class', 'uk-text-left' );
	beans_replace_action_hook( 'beans_site_branding', 'beans_menu_item[_33]_prepend_markup' );
	beans_remove_output( 'beans_menu_item_text_33' );

	beans_add_smart_action( 'beans_header_after_markup', 'get_hero_area' );

	function get_hero_area() {
		if ( is_front_page()  ) {
			return;
		}
		$context = Timber::get_context();
		$queried_object = get_queried_object();
		if ( is_tax() ) {
			$image = get_field( 'hero_image', $queried_object );
			$title = $queried_object->name;
		} elseif(is_category()){
			$title = $queried_object->name;
			$image = get_field('taxo_image',$queried_object);
		}elseif(is_home()){
			$title = 'blog';
			$image = get_field('hero_image_blog','option');
		}elseif(is_archive('testimonials')){
			$title = $queried_object->name;
			$image = get_field('hero_image_testimonials','option');
		} else {
			$image = get_field( 'hero_image' );
			$title = $queried_object->post_title;

		}
		$context['image'] = $image;
		$context['title'] = $title;
		$templates        = array( '_hero-area.twig' );
		Timber::render( $templates, $context );
	}

	beans_add_smart_action( 'beans_header_prepend_markup', 'header_call_info' );

	function header_call_info() {
		$context   = Timber::get_context();
		$templates = array( 'header.twig' );
		Timber::render( $templates, $context );
	}

	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );

}