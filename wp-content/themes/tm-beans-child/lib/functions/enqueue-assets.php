<?php
//// Remove Beans Default Styling
//remove_theme_support( 'beans-default-styling' );
// Enqueue uikit assets

//Dev css
beans_add_smart_action( 'wp_enqueue_scripts', 'wst_enqueue_dev_styles' );
//Prod css, autocompile
beans_add_smart_action( 'beans_uikit_enqueue_scripts', 'wst_enqueue_styles', 5 );


function wst_enqueue_dev_styles() {
	//dev css mode: available to css injection and source maps trough codekit, gulp or grunt
	if(!get_field('wst_css_dev_mode','option')) {
		return;
	}
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/assets/less/css/style.css' );
}
function wst_enqueue_styles(){
//	Prod css mode: autocompile
	if(get_field('wst_css_dev_mode','option')) {
		return;
	}
//	 Enqueue uikit overwrite theme folder
	beans_uikit_enqueue_theme( 'beans_child', CHILD_URL . '/assets/less/initial-theme' );
//Add the theme style as a uikit fragment to have access to all the variables
	beans_compiler_add_fragment( 'uikit', array(
		CHILD_URL . '/assets/less/partials/fonts.less',
		CHILD_URL . '/assets/less/mymixins.less',

		CHILD_URL . '/assets/less/partials/default.less',
		CHILD_URL . '/assets/less/partials/typo.less',
		CHILD_URL . '/assets/less/layouts/text-area.less',
		CHILD_URL . '/assets/less/layouts/four-boxes.less',
		CHILD_URL . '/assets/less/layouts/home-slider.less',
		CHILD_URL . '/assets/less/layouts/spacer.less',
		CHILD_URL . '/assets/less/layouts/why-choose-us.less',
		CHILD_URL . '/assets/less/layouts/video-section.less',
		CHILD_URL . '/assets/less/layouts/featured-clients.less',
		CHILD_URL . '/assets/less/layouts/testimonials.less',
		CHILD_URL . '/assets/less/partials/header.less',
		CHILD_URL . '/assets/less/partials/footer.less',
		CHILD_URL . '/assets/less/partials/nav.less',
		CHILD_URL . '/assets/less/partials/sidebar.less',
		CHILD_URL . '/assets/less/partials/widgets.less',
		CHILD_URL . '/assets/less/partials/content.less',
		CHILD_URL . '/assets/less/partials/pages.less',


	), 'less' );

}
beans_add_smart_action( 'beans_uikit_enqueue_scripts', 'wst_enqueue_uikit_assets', 5 );

function wst_enqueue_uikit_assets() {



	beans_compiler_add_fragment( 'uikit', array(
		CHILD_URL . '/assets/js/animatedtext.js',
		CHILD_URL . '/assets/js/theme.js'
	), 'js' );


	beans_uikit_enqueue_components( array(
//		'contrast',
//		'cover',
//		'animation',
		'modal',
		'overlay',
//		'column',
//		'switcher',
//		'scrollspy'
	) );
	beans_uikit_enqueue_components( array(
		'sticky',
		'slideshow',
		'slider',
//		'lightbox',
		'grid',
//		'parallax',
		'dotnav',
		'slidenav'
	),
		'add-ons' );

}

//google fonts
add_action( 'wp_enqueue_scripts', 'wst_add_google_fonts' );
function wst_add_google_fonts() {

	wp_enqueue_style( 'wst-google-fonts', 'https://fonts.googleapis.com/css?family=https://fonts.googleapis.com/css?family=Arvo:400,700|Source+Sans+Pro:400,400i,600,600i', false );
}