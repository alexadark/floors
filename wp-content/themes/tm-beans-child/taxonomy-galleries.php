<?php
beans_remove_markup( 'beans_archive_title' );
beans_remove_output( 'beans_archive_title_text' );
beans_remove_markup( 'beans_post_header' );
beans_remove_action( 'beans_post_title' );
beans_remove_markup( 'beans_post' );
beans_remove_markup( 'beans_post_body' );
beans_remove_markup( 'beans_post_content' );
beans_remove_action( 'beans_post_image' );
beans_add_smart_action( 'beans_content_prepend_markup', 'get_gallery_filters' );
function get_gallery_filters() {
	$context               = Timber::get_context();
	$context['types']      = Timber::get_terms( 'floor-types' );
	$context['categories'] = Timber::get_terms( 'floor-categories' );
	$templates             = array( 'gallery-filters.twig' );
	Timber::render( $templates, $context );
}

beans_add_smart_action( 'beans_content_prepend_markup', 'get_slideshow_gallery_view' );
function get_slideshow_gallery_view() {
	$context          = Timber::get_context();
	$slug             = get_queried_object()->slug;
	$galleries_tax_query = array(
		'taxonomy' => 'galleries',
		'field'    => 'slug',
		'terms'    => array( $slug ),
	);
	$args             = array(
		'post_type' => 'floors',
	);

	if ( isset( $_GET['floor-types'] ) ) {
		$floor_type = esc_attr( $_GET['floor-types'] );

		$args['tax_query'] = array(
			'relation' => 'AND',
			$galleries_tax_query,
			array(
				'taxonomy' => 'floor-types',
				'field'    => 'slug',
				'terms'    => array( $floor_type ),
			),
		);
	} elseif ( isset( $_GET['floor-categories'] ) ){
		$floor_cat = esc_attr( $_GET['floor-categories'] );

		$args['tax_query'] = array(
			'relation' => 'AND',
			$galleries_tax_query,
			array(
				'taxonomy' => 'floor-categories',
				'field'    => 'slug',
				'terms'    => array( $floor_cat ),
			),
		);

	} else {
		$args['tax_query'] = array( $galleries_tax_query );
	}
	$items            = Timber::get_posts( $args );
	$context['items'] = $items;
	$templates        = array( 'slideshow-gallery.twig' );
	Timber::render( $templates, $context );


}


beans_load_document();