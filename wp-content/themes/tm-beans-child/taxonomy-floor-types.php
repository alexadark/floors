<?php
beans_remove_markup( 'beans_archive_title' );
beans_remove_output( 'beans_archive_title_text' );
beans_remove_markup( 'beans_post_header' );
beans_remove_action( 'beans_post_title' );
beans_remove_markup( 'beans_post' );
beans_remove_markup( 'beans_post_body' );
beans_remove_markup( 'beans_post_content' );
beans_remove_action( 'beans_post_image' );

beans_add_smart_action( 'beans_content_prepend_markup', 'get_slideshow_gallery_type_view' );
function get_slideshow_gallery_type_view() {
	$context          = Timber::get_context();
	$slug             = get_queried_object()->slug;
	$args             = array(
		'post_type' => 'floors',
		'tax_query' => array(
			array(
				'taxonomy' => 'floor-types',
				'field'    => 'slug',
				'terms'    => array( $slug ),
			),
		)
	);
	$items            = Timber::get_posts( $args );
	$context['items'] = $items;
	$templates        = array( 'slideshow-gallery.twig' );
	Timber::render( $templates, $context );


}


 beans_load_document();


