<?php
beans_add_smart_action( 'beans_header_after_markup', 'get_home_layouts' );
function get_home_layouts() {
	$context   = Timber::get_context();
	$templates = array( 'front-page.twig' );
	$context['galleries'] = Timber::get_terms( 'galleries' );
	Timber::render( $templates, $context );
	get_testimonials();
	}

beans_load_document();